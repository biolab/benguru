from django.contrib import admin
from benchmarks.models import *

class FieldAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'value')
    list_filter = ('benchmark', )

admin.site.register(Field, FieldAdmin)

class FieldInline(admin.TabularInline):
    model = Field
    extra = 0

class BenchmarkAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'date', 'transaction_type',
                    'scaling_factor', 'threads', 'clients',
                    'transactions_per_client', 'transactions_processed',
                    'tps_excluding', 'tps_including')
    list_filter = ('user', 'date')
    inlines = [FieldInline]


admin.site.register(Benchmark, BenchmarkAdmin)


class BookmarkAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'user')


admin.site.register(Bookmark, BookmarkAdmin)
