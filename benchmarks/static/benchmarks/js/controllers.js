'use strict';

function BenchmarksCtrl($scope, $filter, $routeParams, $location, ngTableParams, Benchmark, Bookmark) {
    var bookmarkId = $routeParams.bookmarkId,
        field_names = {},
        all_fields = {},
        orderedData,
        data,
        bookmark_loaded = false,
        selected_fields = [],
        filtered_fields = [];

    var default_columns = [],
        default_column_title = function (el) {
            return function () {
                return default_column_names[el][0];
            }
        },
        default_column_names = [
            ['', false],
            ['Title', 'title'],
            ['Date', 'date'],
            ['Tags', false],
            ['Type', 'transaction_type'],
            ['Scaling', 'scaling_factor'],
            ['Threads', 'threads'],
            ['Clients', 'clients'],
            ['Trans. / Client', 'transactions_per_client'],
            ['Trans. process.', 'transactions_processed'],
            ['TPS', 'tps_excluding'],
            ['TPS (conn.)', 'tps_including'],
            ['User', 'username']
        ];

    for (var i = 0, l = default_column_names.length; i < l; i++) {
        default_columns.push({
            id: i,
            title: default_column_title(i),
            sortable: default_column_names[i][1],
            filter: false,
            filterTemplateURL: false,
            headerTemplateURL: false,
            filterData: null,
            show: function () {
                return true;
            }
        });
    }

    $('#benchmarks-list-item').addClass('active');
    $('#bookmarks-list-item').removeClass('active');

    $scope.error_message = '';
    $scope.success_message = '';

    function set_defaults() {
        $scope.tableBenchmarks = new ngTableParams({
            page: 1,            // show first page
            total: 0,           // length of data
            count: 10,          // count per page
            filter: '',         // initial filter
            sorting: {
                date: 'desc'    // initial sorting
            },
            columns: []
        });

        $scope.attribute_x = null;
        $scope.attribute_y = [];
        $scope.all_fields = [];
        $scope.checkboxes = { 'checked': false, items: {}, 'num_checked': 0 };
    }

    function set_attributes() {
        var ax = $scope.attribute_x,
            ay = $scope.attribute_y;

        $scope.attributes_x = [
            {label: 'Date', attribute: 'date'},
            {label: 'Title', attribute: 'title'}
        ];

        $scope.attributes_y = [
            {label: 'TPS', attribute: 'tps_excluding'},
            {label: 'TPS (including connection establishing)', attribute: 'tps_including'},
            {label: 'Transactions per client', attribute: 'transactions_per_client'},
            {label: 'Transactions processed', attribute: 'transactions_processed'},
            {label: 'Threads', attribute: 'threads'},
            {label: 'Clients', attribute: 'clients'},
            {label: 'Scaling factor', attribute: 'scaling_factor'}
        ];

        // add special fields to analysis combo boxes
        for (var i = 0, l = selected_fields.length; i < l; i++) {
            if (field_names[selected_fields[i]] === 'STR') {
                $scope.attributes_x.push({label: selected_fields[i], attribute: selected_fields[i]});
            } else if (field_names[selected_fields[i]] === 'NUM') {
                $scope.attributes_y.push({label: selected_fields[i], attribute: selected_fields[i]});
            }
        }

        // set attributes that were selected in y combo
        if (ay !== undefined) {
            var old_attributes_y = {},
                new_attributes_y = [];

            // build a set of attributes that were selected in y combo
            for (var i = 0, l = ay.length; i < l; i++) {
                old_attributes_y[ay[i].attribute] = true;
            }

            for (var i = 0, l = $scope.attributes_y.length; i < l; i++) {
                if ($scope.attributes_y[i].attribute in old_attributes_y) {
                    new_attributes_y.push($scope.attributes_y[i]);
                }
            }

            $scope.attribute_y = new_attributes_y;
        }

        // set attribute that was selected in x combo
        if (ax !== undefined && ax !== null) {
            for (var i = 0, l = $scope.attributes_x.length; i < l; i++) {
                if ($scope.attributes_x[i].attribute === ax.attribute) {
                    $scope.attribute_x = $scope.attributes_x[i];
                    break;
                }
            }
        }
    }

    function set_bookmark() {
        if (bookmarkId !== undefined) {
            $('#bookmark-button').css("display", "none");
        } else {
            $('#bookmark-button').css("display", "block");
        }

        if (bookmarkId !== undefined && window.disqus_shortname === undefined) {
            setTimeout(function () {
                window.disqus_shortname = 'benguru';
                window.disqus_identifier = bookmarkId;
                window.disqus_url = 'http://benguru.fri.uni-lj.si/benchmarks/' + bookmarkId + '/';

                (function () {
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            }, 1000);
        } else if (bookmarkId !== undefined && window.DISQUS !== undefined) {
            setTimeout(function () {
                window.disqus_identifier = bookmarkId;
                window.disqus_url = 'http://benguru.fri.uni-lj.si/benchmarks/' + bookmarkId + '/';

                DISQUS.reset({
                    reload: true,
                    config: function () {
                        this.page.identifier = window.disqus_identifier;
                        this.page.url = window.disqus_url;
                    }
                });
            }, 1000);
        }
    }

    set_defaults();
    set_attributes();

    $scope.bookmark = {
        title: "",
        description: ""
    }

    $('.multiselect').multiselect({
        buttonClass: 'btn btn-default',
        maxHeight: 240
    });

    $scope.add_bookmark = function () {
        $scope.bookmark.scope = JSON.stringify({
            filter: $scope.tableBenchmarks.filter,
            sorting: $scope.tableBenchmarks.sorting,
            checkboxes: $scope.checkboxes,
            attribute_x: $scope.attribute_x,
            attribute_y: $scope.attribute_y
        });

        if (true) {
            $('#request-demo-email').closest('.control-group').removeClass('error');
            $.ajax({
                type: 'POST',
                url: '/api/v1/bookmark/',
                data: JSON.stringify($scope.bookmark),
                dataType: 'json',
                contentType: 'application/json',
                processData: false
            })
                .complete(function (xhr, status) {
                    $scope.$apply(function () {
                        if (xhr.status !== 201) {
                            $scope.error_message = "Error adding bookmark: " + status + " CODE: " + xhr.status;
                        }
                        else {
                            $scope.bookmark.title = "";
                            $scope.bookmark.description = "";
                            $scope.success_message = "Bookmark added";

                            setTimeout(function () {
                                $('#bookmark-form').slideUp(200);
                                $scope.success_message = "";
                            }, 3000);
                        }
                    });
                });
        }
        else {
            $scope.error_message = "Error adding bookmark";
        }
    }

    $scope.show_bookmark_form = function () {
        var form = $('#bookmark-form');
        form.toggle();
    }

    data = Benchmark.query(function () {
            // process special fields (find and insert in dict)
            for (var i = 0, l = data.objects.length; i < l; i++) {
                data.objects[i].field_set = {};

                for (var j = 0, l2 = data.objects[i].fields.length; j < l2; j++) {
                    if (!(data.objects[i].fields[j].name in all_fields)) {
                        all_fields[data.objects[i].fields[j].name] = data.objects[i].fields[j].type;
                    }
                    data.objects[i].field_set[data.objects[i].fields[j].name] = data.objects[i].fields[j];
                }
            }

            set_attributes();

            // watch for changes in table properties (sort, page...)
            $scope.$watch('tableBenchmarks', function (params) {
                orderedData = params.sorting ?
                    $filter('orderBy')(data.objects, params.orderBy()) :
                    data.objects;

                orderedData = params.filter ?
                    $filter('filter')(orderedData, params.filter) :
                    orderedData;

                params.total = orderedData.length;
                $scope.benchmarks = orderedData.slice((params.page - 1) * params.count, params.page * params.count);
                // move select all checkbox from hidden filter to table header
                $('#select_all').prependTo("table > thead > tr:first > th:first");

                if (params.filter) {
                    var filtered_field_set = {};
                    for (var i = 0, l = orderedData.length; i < l; i++) {

                        for (var j = 0, l2 = orderedData[i].fields.length; j < l2; j++) {
                            if (!(orderedData[i].fields[j].name in field_names)) {
                                filtered_field_set[orderedData[i].fields[j].name] = true;
                            }
                        }
                    }
                    filtered_fields = [];
                    for (var key in filtered_field_set) {
                        filtered_fields.push(key);
                    }
                } else {
                    filtered_fields = [];
                }

                var columns = default_columns.slice(0),
                    fil_title = function (el) {
                        return function () {
                            return filtered_fields[el];
                        }
                    };

                for (var i = 0, l = filtered_fields.length; i < l; i++) {
                    $scope.tableBenchmarks.columns.push({
                        id: $scope.tableBenchmarks.columns.length,
                        title: fil_title(i),
                        sortable: false,
                        filter: false,
                        filterTemplateURL: false,
                        headerTemplateURL: false,
                        filterData: null,
                        show: function () {
                            return true;
                        }
                    });
                }

                $scope.tableBenchmarks.columns = columns;
                $scope.filtered_fields = filtered_fields;
            }, true);

            // watch for check all checkbox
            $scope.$watch('checkboxes.checked', function (value) {
                for (var i in orderedData) {
                    if (angular.isDefined(orderedData[i].id)) {
                        $scope.checkboxes.items[orderedData[i].id] = value;
                    }
                }
            });

            // watch for data checkboxes
            $scope.$watch('checkboxes.items', function (values) {
                var checked = 0, unchecked = 0,
                    total = orderedData.length;

                field_names = {};

                for (var i = 0, l = orderedData.length; i < l; i++) {
                    checked += ($scope.checkboxes.items[orderedData[i].id]) || 0;
                    unchecked += (!$scope.checkboxes.items[orderedData[i].id]) || 0;

                    if ($scope.checkboxes.items[orderedData[i].id]) {
                        for (var j = 0, l2 = orderedData[i].fields.length; j < l2; j++) {
                            if (!(orderedData[i].fields[j].name in field_names)) {
                                field_names[orderedData[i].fields[j].name] = orderedData[i].fields[j].type;
                            }
                        }
                    }
                }
                selected_fields = [];
                for (var key in field_names) {
                    selected_fields.push(key);
                }

                if ((unchecked == 0) || (checked == 0)) {
                    $scope.checkboxes.checked = (checked == total);
                }

                $scope.checkboxes.num_checked = checked;
                set_attributes();
                // grayed checkbox
                $("#select_all").prop("indeterminate", (checked != 0 && unchecked != 0));
                plot_benchmarks();

                if (bookmarkId !== undefined && bookmark_loaded) {
                    bookmarkId = undefined;
                    set_bookmark();
                }
            }, true);

            var plot_benchmarks = function () {
                if ($scope.attribute_x !== null && $scope.attribute_y.length > 0 &&
                    $scope.checkboxes.num_checked > 0) {

                    var counter = 0,
                        ticks = [],
                        plot_data = [],
                        plot_values = [],
                        container = $("#plot_placeholder"),
                        bar_width = 4 / ($scope.attribute_y.length + 0.5) / 5;

                    for (var i = 0, l = orderedData.length; i < l; i++) {
                        if ($scope.checkboxes.items[orderedData[i].id]) {
                            var x = orderedData[i][$scope.attribute_x.attribute],
                                y;

                            if (x === undefined) {
                                if ($scope.attribute_x.attribute in orderedData[i].field_set) {
                                    x = orderedData[i].field_set[$scope.attribute_x.attribute].value;
                                }
                            }
                            for (var j = 0, l1 = $scope.attribute_y.length; j < l1; j++) {
                                y = orderedData[i][$scope.attribute_y[j].attribute];

                                if (y === undefined) {
                                    if ($scope.attribute_y[j].attribute in orderedData[i].field_set) {
                                        y = orderedData[i].field_set[$scope.attribute_y[j].attribute].value;
                                    }
                                }
                                if (plot_values.length <= j) {
                                    plot_values.push([]);
                                }
                                plot_values[j].push([counter + j / ($scope.attribute_y.length + 0.5), y]);
                            }
                            var tick_pos = counter + ((j - 1) / ($scope.attribute_y.length + 0.5) + bar_width) / 2;
                            ticks.push([tick_pos, x]);
                            counter += 1;
                        }
                    }

                    for (var i = 0, l = plot_values.length; i < l; i++) {
                        plot_data.push({
                            label: $scope.attribute_y[i].label,
                            data: plot_values[i],
                            bars: {
                                show: true,
                                align: "left",
                                barWidth: bar_width,
                                fill: true,
                                lineWidth: 1
                            },
                            yaxis: i + 1
                        });
                    }

                    container.css("display", "block");
                    container.css("width", $(".col-md-12:first").width());

                    $.plot(container, plot_data, {
                        xaxis: {
                            ticks: ticks,
                            axisLabel: $scope.attribute_x.label,
                            tickLength: 0
                        },
                        grid: {
                            borderWidth: 1,
                            minBorderMargin: 0,
                            labelMargin: 10,
                            backgroundColor: {
                                colors: ["#fff", "#e4f4f4"]
                            },
                            margin: {
                                top: 30,
                                bottom: 15,
                                left: 20,
                                right: 0
                            },
                            markings: null
                        },
                        legend: {
                            labelBoxBorderColor: "none",
                            backgroundColor: "none",
                            position: "center",
                            noColumns: $scope.attribute_y.length
                        }
                    });

                    var yaxisLabel = $("<div class='axisLabel yaxisLabel'></div>")
                        .text($scope.attribute_y.label)
                        .appendTo(container);

                    // Since CSS transforms use the top-left corner of the label as the transform origin,
                    // we need to center the y-axis label by shifting it down by half its width.
                    // Subtract 20 to factor the chart's bottom margin into the centering.
                    yaxisLabel.css("margin-top", yaxisLabel.width() / 2 - 20);
                }
            }

            $scope.$watch('attribute_x', plot_benchmarks);
            $scope.$watch('attribute_y', plot_benchmarks);

            // load bookmarked values
            if (bookmarkId !== undefined) {
                Bookmark.get({bookmarkId: bookmarkId}, function (bookmark) {
                    set_defaults();

                    bookmark.scope = JSON.parse(bookmark.scope);

                    if (bookmark.scope.filter !== undefined) {
                        $scope.tableBenchmarks.filter = bookmark.scope.filter;
                    }
                    if (bookmark.scope.sorting !== undefined) {
                        $scope.tableBenchmarks.sorting = bookmark.scope.sorting;
                    }
                    if (bookmark.scope.checkboxes !== undefined) {
                        $scope.checkboxes = bookmark.scope.checkboxes;
                    }
                    if (bookmark.scope.attribute_x !== undefined) {
                        $scope.attribute_x = bookmark.scope.attribute_x;
                    }
                    if (bookmark.scope.attribute_y !== undefined) {
                        $scope.attribute_y = bookmark.scope.attribute_y;
                    }
                });
            }
        }
    );

    set_bookmark();
}

function BookmarksCtrl($scope, $filter, ngTableParams, Bookmark) {
    var data,
        orderedData;

    $('#benchmarks-list-item').removeClass('active');
    $('#bookmarks-list-item').addClass('active');

    $scope.tableBookmarks = new ngTableParams({
        page: 1,            // show first page
        total: 0,           // length of data
        count: 10,          // count per page
        filter: '',         // initial filter
        sorting: {
            date: 'desc'    // initial sorting
        }
    });

    data = Bookmark.query(function () {
        // watch for changes in table properties (sort, page...)
        $scope.$watch('tableBookmarks', function (params) {
            orderedData = params.sorting ?
                $filter('orderBy')(data.objects, params.orderBy()) :
                data.objects;

            orderedData = params.filter ?
                $filter('filter')(orderedData, params.filter) :
                orderedData;

            params.total = orderedData.length;
            $scope.bookmarks = orderedData.slice((params.page - 1) * params.count, params.page * params.count);
            // move select all checkbox from hidden filter to table header
            $('#select_all').prependTo("table > thead > tr:first > th:first");
        }, true);

    });
}
