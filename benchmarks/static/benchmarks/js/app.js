'use strict';

var benguruApp = angular.module('benguruApp', ['benguruServices', 'ngTable'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {templateUrl: '/static/benchmarks/partials/benchmarks.html', controller: BenchmarksCtrl}).
            when('/book/', {templateUrl: '/static/benchmarks/partials/bookmarks.html', controller: BookmarksCtrl}).
            when('/:bookmarkId/', {templateUrl: '/static/benchmarks/partials/benchmarks.html', controller: BenchmarksCtrl});
    }])

    .directive('multiselectDropdown', [function () {
        return function (scope, element, attributes) {

            element = $(element[0]); // Get the element as a jQuery element

            // Below setup the dropdown:

            element.multiselect({
                buttonClass: 'btn btn-small',
                buttonWidth: '200px',
                buttonContainer: '<div class="btn-group" />',
                maxHeight: 200,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,

                // Replicate the native functionality on the elements so
                // that angular can handle the changes for us.
                onChange: function (optionElement, checked) {
                    optionElement.removeAttr('selected');
                    if (checked) {
                        optionElement.attr('selected', 'selected');
                    }
                    element.change();
                }

            });
            // Watch for any changes to the length of our select element
            scope.$watch(function () {
                return element[0].length;
            }, function () {
                element.multiselect('rebuild');
            });

            // Watch for any changes from outside the directive and refresh
            scope.$watch(attributes.ngModel, function (option) {
                if (option instanceof Array) {
                    for (var i = 0, l = option.length; i < l; i++) {
                        element.multiselect('selectText', option[i].label);
                    }
                } else if (option !== null) {
                    element.multiselect('selectText', option.label);
                }
                element.multiselect('refresh');
            });

            // Below maybe some additional setup
        }
    }])

    .filter('tag', function () {
        var re = new RegExp(';', 'g');

        return function (text) {
            if (text != '') {
                return '<span class="label label-primary">' + text.replace(re, '</span>&nbsp;<span class="label label-primary">') + '</span>';
            } else {
                return '';
            }

        };
    })

    .filter('field_value', function () {
        return function (input, key) {
            if (key in input) {
                return input[key].value;
            } else {
                return '';
            }
        }
    });
