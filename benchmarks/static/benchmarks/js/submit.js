'use strict';

$(document).ready(function () {
    var upload = $("#Upload");
    var insert = $("#Insert");
    upload.hide();

    $("#select-method").change(function () {
        var str = "";
        $("#select-method option:selected").each(function () {
            if ($(this).text() == 'Upload benchmark results') {
                insert.hide();
                upload.show();
            } else {
                insert.show();
                upload.hide();
            }
        });

    }).trigger('change');

    $("#add-field").click(function () {
        var fields = $("[id|='field']");
        var last = fields[fields.length-1];
        var last_id = parseInt(last.id.replace('field-', ''));

        var clone = '<div id="field-' + (last_id + 1) + '" class="row">' +
            last.innerHTML.replace(new RegExp('form-' + last_id, 'g'), "form-" + (last_id + 1)) +
            '</div>';

        $(last).after(clone);
        $("#id_form-TOTAL_FORMS").attr("value", last_id + 2);
    });
});
