'use strict';

angular.module('benguruServices', ['ngResource'])

    .factory('Benchmark', function ($resource) {
        return $resource('/api/v1/benchmark/', {}, {
            query: {
                method: 'GET',
                isArray: false
            }
        });
    })

    .factory('Bookmark', function ($resource) {
        return $resource('/api/v1/bookmark/:bookmarkId', {}, {
            query: {
                method: 'GET',
                params: {
                    bookmarkId: ''
                },
                isArray: false
            }
        });
    });
