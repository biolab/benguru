from django.conf.urls import url
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

from tastypie import fields
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource

from benchmarks.models import Benchmark, Bookmark, Field


class BenguruBasicAuthentication(BasicAuthentication):
    def __init__(self, *args, **kwargs):
        super(BenguruBasicAuthentication, self).__init__(*args, **kwargs)

    def is_authenticated(self, request, **kwargs):
        if request.method == "GET":
            return True

        if 'sessionid' in request.COOKIES:
            s = Session.objects.get(pk=request.COOKIES['sessionid'])
            if '_auth_user_id' in s.get_decoded():
                u = User.objects.get(id=s.get_decoded()['_auth_user_id'])
                request.user = u
                return True
        return super(BenguruBasicAuthentication, self).is_authenticated(request, **kwargs)


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        detail_uri_name = 'username'
        fields = ['username']
        allowed_methods = ['get', 'post']
        authentication = BenguruBasicAuthentication()
        authorization = DjangoAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]


class FieldResource(ModelResource):
    class Meta:
        queryset = Field.objects.all()
        resource_name = 'field'
        fields = ['id', 'name', 'type', 'value']
        allowed_methods = ['get', 'post']
        authentication = BenguruBasicAuthentication()
        authorization = DjangoAuthorization()

    def hydrate(self, bundle):
        bundle.obj.benchmark = bundle.related_obj
        return bundle


class BenchmarkResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user', full=True, null=True)
    fields = fields.ToManyField(FieldResource, attribute='fields', related_name='field', full=True, null=True)

    class Meta:
        queryset = Benchmark.objects.all()
        resource_name = 'benchmark'
        allowed_methods = ['get', 'post']
        authentication = BenguruBasicAuthentication()
        authorization = DjangoAuthorization()


class BookmarkResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user', full=True, null=True)

    class Meta:
        queryset = Bookmark.objects.all()
        resource_name = 'bookmark'
        allowed_methods = ['get', 'post']
        authentication = BenguruBasicAuthentication()
        authorization = DjangoAuthorization()

    def hydrate_user(self, bundle):
        bundle.obj.user = bundle.request.user
        return bundle
