import os
import json

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth.models import User

from benchmarks.models import Benchmark, Field


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        fn = os.path.join(settings.PROJECT_ROOT, 'benchmarks', 'static', 'benchmarks', 'benchmark-test-data.json')
        data = json.loads(open(fn, 'r').read())
        admin = User.objects.filter(is_superuser__exact=True)[0]
        for d in data:
            d['user'] = admin
            fields = d['fields']
            del d['fields']
            benchmark = Benchmark.objects.create(**d)
            benchmark.save()

            for f in fields:
                f['benchmark'] = benchmark
                field = Field(**f)
                field.save()
