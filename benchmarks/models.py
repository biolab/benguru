from django import forms
from django.db import models
from django.utils import timezone
from django.contrib import admin
from django.contrib.auth.models import User
from django.forms.models import modelformset_factory


class Benchmark(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateTimeField('date of measurement', default=timezone.now)
    user = models.ForeignKey(User)
    tags = models.CharField(max_length=200, blank=True)
    transaction_type = models.CharField(max_length=150, blank=True)
    scaling_factor = models.DecimalField(max_digits=16, decimal_places=8, blank=True, null=True)
    threads = models.IntegerField(verbose_name="Threads", max_length=30, blank=True, null=True)
    clients = models.IntegerField(verbose_name="Clients", max_length=30, blank=True, null=True)
    transactions_per_client = models.IntegerField('Transactions per client', max_length=30, blank=True, null=True)
    transactions_processed = models.IntegerField(max_length=30, blank=True, null=True)
    tps_including = models.DecimalField(max_digits=16, decimal_places=8,
                                        verbose_name="TPS (including connections establishing)", blank=True, null=True)
    tps_excluding = models.DecimalField(max_digits=16, decimal_places=8,
                                        verbose_name="TPS (excluding connections establishing)", blank=True, null=True)

    def __unicode__(self):
        return unicode(self.title)


class BenchmarkForm(forms.ModelForm):
    class Meta:
        model = Benchmark
        exclude = 'user'


class Field(models.Model):
    TYPE_CHOICES = (
        ('STR', 'String'),
        ('NUM', 'Number'),
        ('SER', 'Series'),
        ('FILE', 'File'),
    )
    benchmark = models.ForeignKey(Benchmark, related_name="fields")
    name = models.CharField(max_length=100, blank=False)
    type = models.CharField(max_length=30, blank=False, choices=TYPE_CHOICES)
    value = models.CharField(max_length=1000, blank=False)

    def __unicode__(self):
        return unicode(self.name + ": " + self.value)


class FieldForm(forms.ModelForm):
    class Meta:
        model = Field
        exclude = 'benchmark'


FieldFormSet = modelformset_factory(Field, exclude=('benchmark', ))


class UploadForm(forms.Form):
    upload = forms.FileField(
        label='Select a file (must be in .csv format)',
    )


class Bookmark(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User)
    description = models.TextField()
    scope = models.TextField()

    def __unicode__(self):
        return unicode(self.title)
