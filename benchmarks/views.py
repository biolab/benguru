import os
from benchmarks.models import *
from django.shortcuts import render
from django import http
from django.contrib import messages
from benchmarks import parsedata
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from django.contrib.auth.decorators import permission_required

@permission_required('benchmarks.add_benchmark', raise_exception=True)
def submit(request):
    if request.user.is_authenticated and request.method == 'POST':
        benchmark_form = BenchmarkForm(request.POST)
        fields_form = FieldFormSet(request.POST)
        upload_form = UploadForm(request.POST, request.FILES)
        if benchmark_form.is_valid() and fields_form.is_valid():
            benchmark = benchmark_form.save(commit=False)
            benchmark.user = request.user
            benchmark.save()

            for field in fields_form:
                field = field.save(commit=False)
                if field.name == '' or field.type == '' or field.value == '':
                    continue
                field.benchmark = benchmark
                field.save()
            messages.add_message(request, messages.INFO, "Successful!")
            return http.HttpResponseRedirect('')
        elif upload_form.is_valid():
            data = request.FILES['upload']
            name = data.name
            if name.split(".")[1].lower() == "csv":
                path = default_storage.save('temp/'+name, ContentFile(data.read()))
                path = os.path.join(settings.MEDIA_ROOT, path)
                if parsedata.test(path):
                    messages.add_message(request, messages.INFO, "Successful!")
                    os.remove(path)
            else:
                messages.add_message(request, messages.INFO, "Wrong file format")
            return http.HttpResponseRedirect('')
    else:
        benchmark_form = BenchmarkForm()
        fields_form = FieldFormSet(queryset=Field.objects.none())
        upload_form = UploadForm()

    return render(request, 'benchmarks/submit.html', {
        'benchmark_form': benchmark_form, 'fields_form': fields_form, 'upload_form': upload_form
    })
