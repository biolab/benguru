from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView

from tastypie.api import Api

from main.users import views as user_views
from benchmarks import views as measure_views
from benchmarks.resources import BenchmarkResource, BookmarkResource, UserResource, FieldResource

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(BenchmarkResource())
v1_api.register(BookmarkResource())
v1_api.register(FieldResource())

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name="main/home.html"), name='home'),
    url(r'^benchmarks/$', TemplateView.as_view(template_name='benchmarks/benchmarks.html')),
    url(r'^submit/$', measure_views.submit, name="submit"),
    url(r'^register/$', user_views.RegisterView.as_view(template_name='users/register.html')),
    url(r'^signin/$', user_views.LoginView.as_view(template_name='users/signin.html')),
    url(r'^signout/$', user_views.LogoutView.as_view(), name='signout'),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^learn/$', TemplateView.as_view(template_name="main/learn.html"), name='learn'),
    url(r'^api/', include(v1_api.urls)),
)
