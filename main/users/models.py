from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.mail import mail_admins
from django.contrib.sites.models import Site


@receiver(post_save, sender=User)
def notify_admins(sender, **kwargs):
    if kwargs.get('created', None):
        user = kwargs['instance']
        site = Site.objects.get_current()
        message = """New user just registered: %s

Click on the following link to set his permissions: http://%s/admin/auth/user/%d/

""" % (user.username, site, user.id)

        mail_admins("User registered: %s" % user.username, message)
